# -- coding: utf-8 --

# Copyright 2020 Fluxim AG <olivier.scholder@fluxim.com>

from __future__ import absolute_import

__all__ = ["LL"]
__version__ = '0.0.12'
__author__ = 'Fluxim AG'
__copyright__ = "Copyright 2020, Fluxim AG, Winterthur, Switzerland"
__email__ = "stefano.sem@fluxim.com"
