import numpy as np  
import matplotlib.pyplot as plt
from datetime import datetime, date
import scipy
from pint import UnitRegistry as ureg

def sort_ids(ids, prefix=''):
    g = {}
    elements = []
    for x in ids:
        y = x.split('.')
        k,c = int(y[0]),'.'.join(y[1:])
        elements.append(k)
        if len(y)>1:
            g[k] = g.get(k,[]) + [c]
    sortd_list = []
    for x in sorted(elements):
        sortd_list.append(prefix+str(x))
        if x in g:
            sortd_list += sort_ids(g[x],f'{prefix}{x}.')
    return sortd_list
 
def getChunk(data, chunk):
    chunks = [-1]+[x[0] for x in np.argwhere(np.isnan(data))]
    if chunk>=len(chunks):
        raise ValueError("The data does not contain enough chunk")
    else:
        chunks += [len(data)]
        return data[chunks[chunk]+1:chunks[chunk+1]]

def getJVparams(V,I, pce=True, area=1, light_int=1):
    MPP = np.min(V*I)
    impp = np.argmin(V*I)
    iV = V.argsort()
    Isc = np.interp(0,V[iV],I[iV])
    iI = I.argsort()
    Voc = np.interp(0,I[iI],V[iI])
    if pce:
        return {
            "MPP":MPP,
            "Vmpp":V[impp],
            "Impp":I[impp],
            "Voc":Voc,
            "Isc":Isc,
            "FF":(I[impp]*V[impp])/(Voc*Isc),
            "PCE": MPP/(100*light_int)}
    else:
        return {
            "MPP":MPP,
            "Vmpp":V[impp],
            "Impp":I[impp],
            "Voc":Voc,
            "Isc":Isc,
            "FF":(I[impp]*V[impp])/(Voc*Isc)}

def getArea_32bit(settings):
    """
    The area of the pixel is extracted from the settings. (32 bit version)

    Args:
        settings (np.array): self.__getSample(sample_name)['settings'][pixelIndex]
    Ret:
        area in cm2
    """
    params = [col[0].decode() for col in settings]
    surface_index = params.index("Surface area")
    value = settings[surface_index][1]
    try:
        unit = settings[surface_index][2].decode()
        area = value * ureg.Unit(unit)
        area_cm = area.to('cm**2')
        return area_cm.magnitude, False # No error
    except:
        print("Failed to convert the pixel surface unit.")
        print("PCE will be calculated with the area of 1 cm**2")
        print("The value and unit can be manuallty indicated at pyFluxim.utils.getArea_32bit")
        print("at desired_value and desired_unit")
        desired_unit = "cm**2"
        area = 1 * ureg.Unit(desired_unit)
        return area_cm.magnitude, True # Error

def getArea_64bit(pixel_group):
    """
    The area of the pixel is extracted from the pixel. (32 bit version)

    Args:
        pixel (hdf5 group): self.__getSample(sample_name)[pixelIndex]
    Ret:
        area in cm2
    """
    surface_index = list(pixel_group.attrs['param_name']).index("Surface area")
    value = pixel_group.attrs["param_value"][surface_index]
    try:
        unit = pixel_group.attrs["param_unit"][surface_index]
        area = value * ureg.Unit(unit)
        area_cm = area.to('cm**2')
        return area_cm.magnitude, False # No error
    except:
        print("Failed to convert the pixel surface unit.")
        print("PCE will be calculated with the area of 1 cm**2")
        print("The value and unit can be manuallty indicated at pyFluxim.utils.getArea_32bit")
        print("at desired_value and desired_unit")
        desired_unit = "cm**2"
        area = 1 * ureg.Unit(desired_unit)
        return area_cm.magnitude, True # Error

def getLightInt(data):
    """
    The light intensity is extracted from data.

    Args:
        data (dict): data
    Ret:
        light_int (float): number from 0 to 1
    """
    light_int_index = list(data.attrs.get("name")).index("Light intensity")
    light_int = data.attrs.get("start_values")[light_int_index]
    unit = data.attrs.get("units")[light_int_index]
    if unit == "%":
        light_int = light_int/100.0
    return light_int


def plotJV(res, hyst=True, up=True, down=True, ax=None, **kargs):
    if ax is None:
        ax = plt.gca()
    V = res["voltage"]
    I = res["current"]
    numC = np.sum(np.isnan(V))
    if hyst and numC>1:
        for i in range(2):
            Vx = getChunk(V,i)
            UP = Vx[0]<Vx[-1]
            if UP and up or not UP and down:
                ax.plot(Vx, getChunk(I,i)*1e3, label=["down","up"][UP])
    else:
        ax.plot(V, I, label=kargs.get("label",""))
    ax.set_xlabel("Voltage [V]")
    ax.set_ylabel("Current [mA]")
    return ax
    
def flux2timestamp(ts):
    """
    convert a flux time stamp tuple to a python tuple.
    
    return (datetime object, fractional seconds)
    """
    return (datetime.fromtimestamp(ts[1]-2082844800), ts[0]/2**64)
    
def PT100res2temp(Rt, R0=100):
    """
    convert the resistivity measured of a PT100 to its equivalent temperature (in °C)
    """
    
    A = 0.003909
    B = -5.775e-7
    return (np.sqrt(A**2-4*B*(1-(Rt/R0)))-A)/(2*B)