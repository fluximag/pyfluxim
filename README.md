[![Python 3.6](https://img.shields.io/badge/python-3.4+-orange.svg)](https://www.python.org/download/releases/3.4.0/)

# pyFluxim
pyFluxim is a python library to read and handle data saved by instruments from Fluxim AG.
With it you can read and parse Litos Lite results data as well as our new flux data.

## Supported Hardware
* Litos Lite

## Documentation
There is no extensive documentation for the moment. Please have a look at the following examples to get started:

* [Litos Lite](https://nbviewer.jupyter.org/urls/bitbucket.org/fluximag/pyfluxim/raw/c4de023f87290127773239484d74d63a10d7798d/doc/LL_data_example1.ipynb)
* [Flux data](https://nbviewer.jupyter.org/urls/bitbucket.org/fluximag/pyfluxim/raw/15a88fc6a21f10e7cafb4141c45c00e39f514574/doc/Flux_data_example1.ipynb)

